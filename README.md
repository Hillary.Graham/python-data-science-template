# Python Data Science - Template

Template for setting up a Python Data Science development environment with Gitpod.

## Use

1. [Fork repository](https://gitlab.com/alping/python-data-science-template/-/forks/new)
2. [Open in Gitpod](https://gitpod.io/workspaces)

## Directory Structure

    ├── .vscode             <- Directory for VSCode settings
    ├── data                <- Directory for storing data
    ├── src                 <- Directory for Python script files
    │   ├── config.py       <- A place to store config variables
    │   └── examples        <- Collection of example scripts
    ├── .env                <- Specifies environmental variables
    ├── .gitignore          <- Files to be ignored by version control
    ├── .gitpod.yml         <- Gitpod Workspace configuration
    ├── environment.yml     <- Specifies the Python environment/packages
    ├── README.md           <- Instructions for use (this file)
    └── setup.py            <- Makes `src` act as a local package
